# Misc-Utils

## Random small utilities I wrote not worth their own repos.

Try running with `--help` to print the help for each one.

- `list_chrome_sessions`: List URLs currently open in Chrome/Chromium browser
- `mysql_history`: Dump your .mysql_history file as plain text
- `recentfiles`: Mark a file as Recently Used in the freedesktop.org recently used list, making it appear in the Recently Used menus of your GUI (Linux/freedesktop-compliant systems only).
- `shuffle`: randomly shuffle arguments or lines of a file/stdin

### list_chrome_sessions 

A tool to list what URLs (and titles) you have open in chrome/chromium currently.

```
usage: list_chrome_sessions [-h] [--session-dir SESSION_DIR]

Try to parse through the pickled chromium session file and list all currently active tags. URL, title are printed, which is easily parsed
(e.g. url, title = line.split(' ', 1).

options:
  -h, --help            show this help message and exit
  --session-dir SESSION_DIR
                        Path to session dir; by default looks for $HOME/.config/chromium/Default/Sessions and similar under
                        $HOME/.config/{google-chrome,chrome}
```

### recentfiles

Add the specified file(s) to the Recent Files list on any freedesktop-compliant system (including Gnome/KDE/XFCE and most Linux systems). Useful if you have some non-GUI way of generating files but would then like to tag them so they show up in the Recently Used menus of your GUI apps.

```
usage: Add a file or files to the Recent Files list on any Freedesktop.org compliant system (e.g. most Linux systems), and update the mtime of the file
       [-h] [--filename FILENAME] [--nomtime] [add ...]

positional arguments:
  add

options:
  -h, --help            show this help message and exit
  --filename FILENAME, -f FILENAME
                        Use a specified XML file instead of the default
  --nomtime             Don't update the mtime

```

### mysql_history

```
usage: mysql_history [-h] [filename]

Dump your .mysql_history file in plain text to stdout

positional arguments:
  filename    Name of mysql history file, default $HOME/.mysql_history

options:
  -h, --help  show this help message and exit
```

### shuffle 

A tool to print the input (either command-line arguments or lines from stdin or a file) in random order. Useful for randomizing play order, e.g. `find ~/music/ -name '*.mp3' -print0 | shuffle -Z0 --limit 25 | xargs -0 mplayer`

```
usage: shuffle [-h] [--file FILE] [--limit LIMIT] [--quote] [--zero] [--in-zero] [args ...]

Shuffle the input and print in random order. If ARGS are specified, the arguments are shuffled. Otherwise, lines are read from stdin or the
--file/-f source and shuffled.

positional arguments:
  args                  If specified, ARGS are shuffled as output.

options:
  -h, --help            show this help message and exit
  --file FILE, -f FILE  Read input from FILE rather than stdin.
  --limit LIMIT, -l LIMIT
                        Print only LIMIT outgoing values
  --quote, -q           Quote around outgoing words in a shell-friendly manner
  --zero, -0            Separate output with NUL characters rather than newlines
  --in-zero, -Z         Split incoming text on NUL characters rather than newlines

The --zero/--in-zero options are useful in combination with 'xargs -0' or 'find -print0' or similar.
```

